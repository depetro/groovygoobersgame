﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroovyGoobersGameV2
{
    class Character
    {

        public string CharacterName { get; set; }
        public string CharacterDescription { get; set; }
        public string CharacterAbility { get; set; }
        public string CharacterStats { get; set; }

        public Character()
        {

        }

        public Character(string characterName, string characterDescription, string characterAbility, string characterStats)
        {
            CharacterName = characterName;
            CharacterDescription = characterDescription;
            CharacterAbility = characterAbility;
            CharacterStats = characterStats;
        }

        public void toString()
        {
            Console.WriteLine("Name:" + CharacterName);
            Console.WriteLine("Description:" + CharacterDescription);
            Console.WriteLine("Ability:" + CharacterAbility);
            Console.WriteLine("Stats:" + CharacterStats);
        }
    }
}
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
 *  ---------------------------------------------------------------------------
 *  File name: Puzzle.cs
 *  Project name: GroovyGoobers-Game
 *  ---------------------------------------------------------------------------
 *  Creator's name: Amber Stanifer, stanifera@etsu.edu
 *  Course:  CSCI 4250-001
 *  Creation Date: 3-4-2021
 *  ---------------------------------------------------------------------------
 */
namespace GroovyGoobersGameV2
{
    /**
    * Class Name: Puzzle <br>
    * Class Purpose: This class contains several arrays containing information about the puzzle cards name an descriptions 
    *
    * <hr>
    * Date created: 3-4-2021 <br>
    * Date last modified: 3-4-2021
    * @author Amber Stanifer
    */
    //Edited by Micah DePetro to make properties for ease of use to other classes
    public class Puzzle
    {

        public Puzzle()
        {
            PuzzleIndex = 0;
        }
        //added/edited by Micah
        public int PuzzleIndex { get; set; }
        public string[,] PuzzleScenarios = new string[,]
        
        {
            { "Tier 1 Puzzle", "The tier 1 puzzle is the easiest but will call for the player to run through a set of timed doors. " +
                    "The puzzle can be completed by the player if they use an extra amount of stamina to move through them quicker than the doors can close, therefore completing the puzzle and gaining entry." },
         
            { "Tier 2 Puzzle", "This puzzle requires the Player to match several symbols on a wall to the symbols on a totem and pull a lever to continue. " },
            { "Tier 3 Puzzle", "This puzzle requires the player to find a pattern in the room that gives them passage through, the whole floor in the room is covered in pressure plates and if the player steps on the wrong one, they take a significant amount of damage. " +
                    "The way this tile will works is the player will be given an image to and a riddle to select, the player must select the right path according to the given riddle to pass through and complete the puzzle." },
            { "Tier 4 Puzzle", "the player is given to images in which one is not like the other. The player must find the difference between the two images and input what is missing. " +
                    "The trick in this puzzle is that one of the images is part of a mirrored world where something is missing, if the player can point out what is missing, they get to continue. " }
        };

        public string[,] getPuzzle()
        {
            return PuzzleScenarios;
        }

    }
}
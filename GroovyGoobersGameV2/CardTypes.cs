﻿using System;
namespace GroovyGoobersGameV2
{
    public enum CardTypes
    {
        LOOT,
        PUZZLE,
        REPAIR,
        ENCOUNTER,
        BLANK
    }
}

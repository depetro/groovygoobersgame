using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
 *  ---------------------------------------------------------------------------
 *  File name: Encounter.cs
 *  Project name: GroovyGoobers-Game
 *  ---------------------------------------------------------------------------
 *  Creator's name: Amber Stanifer, stanifera@etsu.edu
 *  Course:  CSCI 4250-001
 *  Creation Date: 3-3-2021
 *  ---------------------------------------------------------------------------
 */
namespace GroovyGoobersGameV2
{
    /**
    * Class Name: Encounter <br>
    * Class Purpose: This class contains several arrays containing information about the encounter card tiers, descriptions, and names
    *
    * <hr>
    * Date created: 3-3-2021 <br>
    * Date last modified: 3-3-2021
    * @author Amber Stanifer
    */

    public class Encounter
    {

        public Encounter()
        {
            EncountersIndex = 0;
        }
        //added/edited by Micah
        public int EncountersIndex { get; set; }
        //Edited by Micah DePetro to make properties for ease of use to other classes
        public string[,] EncounterEasy = new string[,]
        {
            { "Goblin Attack", "You've encounter a a goblin, Dobs, he has 15 hit points and a max hit of four." },
            { "Rat Attack", "You've encountered a giant rat, Frank, he has 20 hit points. His max hit is a four." },
            { "Zombie Attack", "You've encountered a zombie, Tainter, he has 25 hit points. His max hit is a four." }
        };

        public string[,] EncouterMedium =  new string[,] 
        {
            { "Werewolf Attack", "You've encountered a werewolf, Ralphie, he has 45 hit points. His max hit is a seven." },
            { "Cyclops Attack", "You've encountered a cyclops, Ivor, he has 50 hit points. His max hit is a seven." },
            { "Vampire Attack", "You've encountered a vampire, Dracula, he has 55 hit points. His max hit is a seven." }
        };
        public string[,] EncounterHard = new string [,]
       
        //Hard Encounter Card Array
        {
            { "Hellhound Attack", "You've encountered a hellhound, Ayala, she has 80 hit points. Her max hit is a ten." },
            { "Hydra Attack", "You've encountered a hydra, Dyani, she has 85 hit points. Her max hit is a ten." }
        };

        public string[,] EncounterBoss = new string [,]
        //Boss Encounter Card Array
        {
            { "Boss Battle", "Oh no! You've awoken a dragon, Brutus, he 130 hit points. His max hit is a fifteen." }
        };

        public string[,] getEncounterEasy()
        {
            return EncounterEasy;
        }

        public string[,] getEncounterMedium()
        {
            return EncouterMedium;
        }
        public string[,] getEncounterHard()
        {
            return EncounterHard;
        }
        public string[,] getEncounterBoss()
        {
            return EncounterBoss;
        }
    }
}
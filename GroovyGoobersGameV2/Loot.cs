﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroovyGoobersGameV2
{
    /**
    * Class Name: Loot <br>
    * Class Purpose: This class holds the different loot card titles with their description inside a 2d array<br>
    *
    * <hr>
    * Date created: 03/03/2021 <br>
    * Date last modified: 03/03/2021
    * @author Maria Elyssa Llavan
    */
    class Loot
    {
        public Loot()
        {
            LootIndex = 0;
        }
        //added/edited by Micah
        public int LootIndex { get; set; }
        //edited by Micah DePetro to create a property for use in other classes.
        public string[,] LootScenarios = new string[,]

        //2d array holding the loot card titles and descriptions
        {
            {"Bucky’s Scimitar",  "A sharp, worn buccaneers sword encrusted with treasures and stung by the salty seas. "},
            {"Tarnoff’s Bionic Boots", "Mechanical boots that rival the tinkering mastery of the dwarves."},
            {"Ancient Scroll of Bailes", "Summons a Storm Atronach to fight for you for the length of 1 encounter"},
            {"Hall’s Holy Hand Grenade", "A Divine weapon of Legend that causes destruction on a Godly level."},
            {"Rezwana’s Clairvoyance Amulet", "A shining silver amulet from days past that shines light on your path to your desires."},
            {"Kawsar’s Magic Mirror", "A round antique mirror that makes the user’s enemy reap what they sow."},
            {"Vial of Desjardins", "This vial is a creation from the tome of Loki, giving the user problem solving skills of a God. This can be only used once, so use it wisely"},
            {"Potion of Erdin", "A sweet tasting potion filled with medicinal ingredients from across the realm, to deliver pure healing to any who drink it"}
        
        };

        public string[,] getLoot()
        {
            return LootScenarios;
        }
    }
}

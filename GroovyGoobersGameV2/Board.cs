﻿
using System;
using System.IO;
using System.Linq;
//hello 
namespace GroovyGoobersGameV2
{
	/// <summary>
	/// Summary description for Class1
	/// </summary>
	public class Board
	{
		
	
		public Tile[,] TileList { get; set; }

		public Deck Cards { get; set; }

		Tile tile = new Tile();

		public Board()
		{
			Cards = null;
			TileList = new Tile[4, 4];

		}

		public Board(Deck d, Tile[,] tl)
		{
			Cards = d;
			TileList = tl;
		}

	
		

	}
}

﻿using System;
using System.Drawing;
 
namespace GroovyGoobersGameV2
{
    public class Tile
    {
        public Card CurrentCard { get; set; }

        public int[] Coordinates { get; set; }

        public Image Picture { get; set; }

        Card c = new Card(); 

        public Tile()
        {
            CurrentCard = new Card();
            Coordinates = new int[2];
            Picture = null;
        }

        public Tile(Card c, int[] coord, Image i)
        {
            CurrentCard = c;
            Coordinates = coord;
            Picture = i;
        }

    }
}

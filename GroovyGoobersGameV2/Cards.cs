﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace GroovyGoobersGameV2
{
    /// <summary>
    /// Contains methods to generate and maintain card objects
    /// </summary>
    public class Card
    {
        /// <summary>
        /// Holds the enum card type value
        /// </summary>
        public CardTypes CardType { get; set; }

        /// <summary>
        /// Holds the title of the card
        /// </summary>
        public string CardTitle { get; set; }

        /// <summary>
        /// Holds the description of the card
        /// </summary>
        public string Description { get; set; }

        

        Random r = new Random();
        
        /// <summary>
        /// empty constructor
        /// </summary>
        public Card()
        {
            CardType = CardTypes.BLANK;
            CardTitle = "";
            Description = "";
        }

        /// <summary>
        /// Parameterized Constructor
        /// </summary>
        /// <param name="type"></param>
        /// <param name="cardTitle"></param>
        /// <param name="description"></param>
        public Card(CardTypes type, string cardTitle, string description)
        {
            CardType = type;
            CardTitle = cardTitle;
            Description = description;
        }

         /// <summary>
         /// Generates a random type from the list of types 
         /// </summary>
         /// <returns></returns>
        public CardTypes getRandomType()
        {
            CardTypes[] values = { CardTypes.BLANK, CardTypes.ENCOUNTER, CardTypes.LOOT, CardTypes.PUZZLE, CardTypes.REPAIR };
            int type = r.Next(values.Length);
            return values[type];
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroovyGoobersGameV2
{
   
    public class Deck
    {
        public List<Card> CurrentDeck { get; set; }
        const int CARDTOTAL = 16;
        Card card = new Card();
        Loot loot = new Loot();
        Repair repair = new Repair();
        Encounter encounter = new Encounter();
        Puzzle puzzle = new Puzzle();
        

        bool cardMade = false;
        public Deck()
        {
            CurrentDeck = new List<Card>();
        }

        public Deck(List<Card> currentDeck)
        {
            CurrentDeck = currentDeck;
        }

        public void addCards()
        {
            for(int i = 0; i < CARDTOTAL; i++)
            {
                card = generateCard();
                CurrentDeck.Add(card);
            }
           
        }

        public Card generateCard()
        {
            Card c = new Card();
            c.CardType = c.getRandomType();
            string[,] array;
            //depending on type get description and title from 
            while (cardMade == false)    //we dont want to reuse scenarios so after we have used all of one scenario dont make another 
            {                           //card of that scenario, try again 
                switch (c.CardType)
                {
                    case CardTypes.LOOT:
                        if (loot.LootIndex < (loot.LootScenarios.Length / 2) - 1)
                        {
                            array = loot.getLoot();
                            c.CardTitle = array[loot.LootIndex, 0];
                            c.Description = array[loot.LootIndex, 1];
                            loot.LootIndex++;
                            cardMade = true;
                        }
                        else
                        {
                            c.CardType = c.getRandomType();
                        }

                        break;

                    case CardTypes.ENCOUNTER:
                        //starts with easy encounters and then we use more difficult, save boss battle for later
                        if (encounter.EncountersIndex < 3)
                        {
                            array = encounter.getEncounterEasy();
                            c.CardTitle = array[encounter.EncountersIndex, 0];
                            c.Description = array[encounter.EncountersIndex, 1];
                            encounter.EncountersIndex++;
                            cardMade = true;
                        }
                        else if (encounter.EncountersIndex > 2 && encounter.EncountersIndex < 6)
                        {
                            array = encounter.getEncounterMedium();
                            c.CardTitle = array[encounter.EncountersIndex - 3, 0];  //subtracts the easy encounters amount to get the 0 and 1 index
                            c.Description = array[encounter.EncountersIndex - 3, 1];
                            encounter.EncountersIndex++;
                            cardMade = true;
                        }
                        else if (encounter.EncountersIndex < 8)
                        {
                            array = encounter.getEncounterHard();
                            c.CardTitle = array[encounter.EncountersIndex - 6, 0];  //subtracts the easy + medium encounters amount to get the 0 and 1 index
                            c.Description = array[encounter.EncountersIndex - 6, 1];
                            encounter.EncountersIndex++;
                            cardMade = true;
                        }
                        else
                        {
                            c.CardType = c.getRandomType();
                        }

                        break;

                    case CardTypes.BLANK:                                  //When a no scenario is selcted
                        c.CardTitle = "Nothing to see here...";
                        c.Description = "Keep it moving...better things ahead...";
                        cardMade = true;
                        break;

                    case CardTypes.REPAIR:                                 //gets repair scenario in order from easiest to hardest
                        if (repair.RepairIndex < (repair.RepairScenarios.Length/2) - 1)
                        {
                            array = repair.getRepairs();
                            c.CardTitle = array[repair.RepairIndex, 0];
                            c.Description = array[repair.RepairIndex, 1];
                            repair.RepairIndex++;
                            cardMade = true;
                        }
                        else
                        {
                            c.CardType = c.getRandomType();
                        }

                        break;

                    case CardTypes.PUZZLE:                                //gets puzzle scenario in order from easiest to hardest
                        if (puzzle.PuzzleIndex < (puzzle.PuzzleScenarios.Length / 2) - 1) 
                        {
                            array = puzzle.getPuzzle();
                            c.CardTitle = array[puzzle.PuzzleIndex, 0];
                            c.Description = array[puzzle.PuzzleIndex, 1];
                            puzzle.PuzzleIndex++;
                            cardMade = true;
                        }
                        else
                        {
                            c.CardType = c.getRandomType();
                        }

                        break;

                    default:

                        break;

                }

            }
            cardMade = false;
            return c;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 *  ---------------------------------------------------------------------------
 *  File name: Repair.cs
 *  Project name: Groovy Goobers Game
 *  ---------------------------------------------------------------------------
 *  Creator's name: Malik Minter, minterm@etsu.edu
 *  Course:  CSCI 4250-001
 *  Creation Date: 3/3/2021
 *  ---------------------------------------------------------------------------
 */

namespace GroovyGoobersGameV2
{
    /// <summary>
    /// Class Name: Repair
    /// Class Purpose: This class will hold a 3D array that will have questions for each repair scenario, answers to choose from, and the correct answers.
    ///                
    /// Date created: 3/3/2021
    /// Last modified: Malik Minter, 3/3/2021, minterm@etsu.edu
    /// @author Malik Minter
    /// </summary>
    ///
    public class Repair
    {
        public Repair()
        {
            RepairIndex = 0;
        }
        //added/edited by Micah
        public int RepairIndex { get; set; }
        //A 3D array that will hold questions for each repair scenario and the answers
        public string[,] RepairScenarios = new string[,]
 
            //Repair Scenario Answers
            {
                //Question 1
                {"Level 1 Bronze Repair", "The following statement has an error: 'ArrayList<int>> listOne = new ArrayList<int>()' What would fix the statement? "},
                //Question 2 Here
                {"Level 2 Silver Repair", "Identify the error with this statement: int[2,3] = { {'1','2','3'}, {4,5,6} }"},
                //Question 3
                { "Level 3 Gold Repair", "True or False, a Struct is a value type in C# that is usually used to hold small amounts of data. " +
                    "A struct can be abstract or inherited by other types " },
                //Question 4
                {"Level 4 Diamond Repair", "A Touple can be used to return multiple values from a function in C#. What are the errors in the method below: " +
                 "private static void Tuple<int, int, int> AddSubMult(int a, int b, double c)" +
                     "var tup1 = new Tuple<int, int, int>(a + b + c, a - b - c, a * b * c);" +
                      "return tup1;"}
            };

        public string[,] getRepairs()
        {
            return RepairScenarios;
        }
            //Answers to choose from for each scenario
           // {
            //    {"a. Remove the extra '>' on the left side of the statement", "b. add an extra ',' after the t in ArrayList"},
            //{"a. Nothing is wrong with it. This is a trick!", "b. the first array is strings when it should be integers."},
             //   {"True", "False"},
              //  {"a. The word var should be replaced with Tuple", "b. Param c should be an integer and the word void should removed from the header"}
           // },
            //Correct answers to each scenario
            //{
                //{"1", "a. Remove the extra '>' on the left side of the statement", "b. add an extra ',' after the t in ArrayList"},
                //{"2", "a. Nothing is wrong with it. This is a trick!", "b. the first array is strings when it should be integers."},
                //{"3", "False"},
                //{"4", "b. Param c should be an integer and the word void should removed from the header"}
            //}
        

    }//end of Repair class
}
